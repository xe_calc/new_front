# Фронтенд для xecalc.ru. Основан на фреймворке nuxt.js и ui фреймворке vuetify

## Сборка для локальной разработки

```bash
# Сборка образа
$ docker build -f docker/dev.dockerfile . -t xecalc_front

# Запуск контейнера
$ docker run --name xecalc_front  --rm -v $(pwd):/app -v /app/node_modules  -ti -p 8080:3000 xecalc_front npm run dev
```

## Сборка для production через docker-compose

```bash
docker build -f docker/production.dockerfile . -t xecalc_front_production --build-arg sentry_dsn={dsn}
# -d для звпуска в фоне
docker-compose -f docker/docker-compose-production.yml up --build
```
