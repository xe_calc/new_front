import {DishItem} from "~/models/dishItem.js";

const STORAGE_KEY = 'dish'


export default {
    getItems() {
        if (!process.client) {
            return []
        }
        let rawItems = JSON.parse(localStorage.getItem(STORAGE_KEY)) || []
        let dishItems = []

        if (rawItems && Array.isArray(rawItems)) {
            rawItems.forEach(function(object, index) {
                dishItems.push(new DishItem(
                    object.id, 
                    object.name, 
                    object.portionName, 
                    {
                        carbCount:  Number(object.carbCount),
                        proteinCount:  Number(object.proteinCount),
                        fatCount:  Number(object.fatCount),
                        caloriesCount:  Number(object.caloriesCount),
                    },
                    Number(object.count),
                    object.isMeasured,
                    object.portionId,
                    object.productId,
                ))
            })
            return dishItems
        }
        return []
    },

    saveItems(items){
        localStorage.setItem(STORAGE_KEY, JSON.stringify(items));
    },

    addItem(productName, portionName, portionData, count, isMeasured, portionId, productId) {
        let items = this.getItems()

        let u = URL.createObjectURL(new Blob([""]))
        URL.revokeObjectURL(u);
        
        let newItem = new DishItem(u.split("/").slice(-1)[0], productName, portionName, portionData, count, isMeasured, portionId, productId)
        items.push(newItem)
        this.saveItems(items)
    },

    getItemById(id){
        let items = this.getItems()
        items.forEach(function(index, object) {
            if (object.id === id) {
                return object
            }
        })
    },

    removeItemById(id) {
        let items = this.getItems()
        items.forEach(function(index, object) {
            if (object.id === id) {
                items.splice(index, 1)
                return
            }
        })
        this.saveItems(items)
        return items
        
    },

    updateItem(id, productName, portionName, portionData, count) {
        let items = this.getItems()
        self = this
        items.some(function(item, index) {
            if (item.id === id) {
                item.productName = productName
                item.portionName = portionName
                item.carbCount = portionData.carbCount
                item.proteinCount = portionData.proteinCount
                item.fatCount = portionData.fatCount
                item.caloriesCount = portionData.caloriesCount
                item.count = count
                items[index] = item
                self.saveItems(items)
                return true
            }
        })
    },

    deleteAll() {
        this.saveItems([])
    }
}