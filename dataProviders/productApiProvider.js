import axios from 'axios'


const client = axios.create({
    baseURL: process.env.productApiUrl,
});

async function searchProducts(phrase, page) {
    const productsPerPage = 20

    if (isNaN(page)) {
        page = 1
    } else {
        page = Number(page)
    }

    if (page === 0){
        page = 1
    }

    let params = {
        'name': phrase,
        'offset': ((page - 1) * productsPerPage).toString(),
        'limit': productsPerPage.toString()
    }

    return client.get('/products/search/', {params: params})
}

async function getPortions(slug) {
    return client.get('/products/'+ slug +'/product-with-portions/')
}

async function sendFeedback(contacts, text) {
    return client.post('/feedback/', {'contacts': contacts, 'text': text})
}

async function sendComplaint(productSlug, portionId, text) {
    return client.post('/products/create-complaint/', {'product': productSlug, 'portion': portionId, 'text': text})
}

async function loadBrandProducts(brandSlug, page) {
    if (!page){
        page=1
    }
    return client.get('/products/brand/' + brandSlug + '/', {params: {'page': page}})
}

export {searchProducts, getPortions, sendFeedback, sendComplaint, loadBrandProducts}
