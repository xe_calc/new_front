FROM node:lts-alpine
RUN apk --no-cache add curl
WORKDIR /app

ADD package.json .

RUN npm install

ENV HOST 0.0.0.0
EXPOSE 8080