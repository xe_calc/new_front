FROM node:lts as builder

WORKDIR /app

COPY . .

RUN ls -l

RUN npm install \
  --prefer-offline \
  --frozen-lockfile \
  --non-interactive \
  --production=false

ENV BASE_URL https://api.xecalc.ru/api/

ARG sentry_dsn
ENV SENTRY_DSN=$sentry_dsn

RUN npm run build

RUN rm -rf node_modules && \
  NODE_ENV=production npm install \
  --prefer-offline \
  --pure-lockfile \
  --non-interactive \
  --production=true

FROM node:lts

WORKDIR /app

COPY --from=builder /app  .

ENV NODE_ENV production
ENV HOST 0.0.0.0