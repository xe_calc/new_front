import {calcParam, carbohydratesToXe} from '~/models/utils.js'

class DishItem {

    constructor(id, productName, portionName, portionData, count, isMeasured, portionId, productId) {
      this.id = id;
      this.name = productName;
      this.portionName = portionName;
      
      this.carbCount = portionData.carbCount
      this.proteinCount = portionData.proteinCount
      this.fatCount = portionData.fatCount
      this.caloriesCount = portionData.caloriesCount
      this.count = count
      this.portionId = portionId
      this.productId = productId

      if (!isNaN(isMeasured)) {
        this.isMeasured = isMeasured
      } else {
        this.isMeasured = true
      }
      
    }
  
    get params() {
      return {
          'xe': this.xeCount,
          'carbohydrates': calcParam(this.carbCount, this.count, this.isMeasured),
          'calories': calcParam(this.caloriesCount, this.count, this.isMeasured),
          'fat': calcParam(this.fatCount, this.count, this.isMeasured),
          'protein': calcParam(this.proteinCount, this.count, this.isMeasured),
      };
    }
  
    get portionNameWithLabel() {
      if (this.isMeasured) {
        return this.count + ' ' + this.measuredPortionLabel
      } else {
        return this.count + ' x ' + this.portionName 
      }
        
    }

    get measuredPortionLabel() {
      let result = ''
      if (this.isMeasured) {
        let measureLabelWords = this.portionName.split(' ')
        result = measureLabelWords[measureLabelWords.length - 1]
      } 
      return result
    }

    get xeCount() {
      return carbohydratesToXe(calcParam(this.carbCount, this.count, this.isMeasured))
    }
}

export {DishItem}