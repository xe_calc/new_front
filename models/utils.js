function calcParam(paramCount, count, isMeasured) {
    if (isMeasured === undefined) {
        isMeasured = false
    }
    if (isMeasured) {
        return (paramCount * count) / 100
    } else {
        return paramCount * count
    }
}

function carbohydratesToXe(carbohydratesCount) {
    return carbohydratesCount / 12
}

function formatFloat(value, numAfterPoint) {
    let valueAsFloat = Number(value)
    if (valueAsFloat !== undefined) {
        return valueAsFloat.toFixed(numAfterPoint)
    }
    
}


export {calcParam, carbohydratesToXe, formatFloat}