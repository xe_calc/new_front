export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'xe_calc_front',
    htmlAttrs: {
      lang: 'ru'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'apple-touch-icon', size: '180x180', href: '/fav/apple-touch-icon.png' },
      { rel: 'icon', type: 'image/png', size: '32x32', href: '/fav/favicon-32x32.png' },
      { rel: 'icon', type: 'image/png', sizes: "16x16", href: '/fav/favicon-16x16.png' },
      { rel: 'icon', type: 'image/x-icon', href: '/fav/favicon-32x32.png' },
      { rel: 'manifest', href: '/fav/site.webmanifest' },
      { rel: 'mask-icon', color: '#5bbad5', href: '/fav/safari-pinned-tab.svg' },
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/assets/styles.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/vuelidate.js',
    '~/plugins/filters.js',
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/yandex-metrika',
    '@nuxtjs/sentry',
  ],

  publicRuntimeConfig: {
    yandexMetrika: {
      id: '70433236',
      clickmap:true,
      trackLinks:true,
      accurateTrackBounce:true,
      webvisor:true
    },
    sentry: {
      dsn: process.env.SENTRY_DSN,
    }
  },
  

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      light: true,
      themes: {
        light: {
          primary_teal: '#67D4D5',
        }
      }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },

  serverMiddleware: [],

  env: {
    productApiUrl: process.env.BASE_URL || 'http://0.0.0.0:8000/api/'
  },
  
  router: {
    trailingSlash: true,
    extendRoutes(routes, resolve) {
      for (let route of routes) {
        if (route.path === '/calc/:id/') {
          route.path = '/products/:id([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})/'
        }
      }
    }
  },

  static: {
    prefix: true
  }
}
