import Vue from 'vue'
import {formatFloat} from '~/models/utils.js'

Vue.filter('formatFloat', (value, numAfterPoint) => {
    return formatFloat(value, numAfterPoint)
})