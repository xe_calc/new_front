import DishLocalStorageProvider from "~/dataProviders/localStorageProvider.js";

export const state = () => ({
  items: [],
  totals: {
    "protein": 0.0,
    "carbCount": 0.0,
    "fat": 0.0,
    "calories": 0.0,
    "xe": 0.0,
  }
})
  
export const mutations = {
  getItems(state) {
    state.items = DishLocalStorageProvider.getItems()
  },

  addItem(state, { productName, portionName, portionData, count, isMeasured, portionId, productId}) {
    DishLocalStorageProvider.addItem(productName, portionName, portionData, count, isMeasured, portionId, productId)
    state.items = DishLocalStorageProvider.getItems()
  },

  changeItem(state, { id, productName, portionName, portionData, count }) {
    DishLocalStorageProvider.updateItem(id, productName, portionName, portionData, count)
    state.items = DishLocalStorageProvider.getItems()
  },

  remove(state, { id }) {
    DishLocalStorageProvider.removeItemById(id)
    state.items = DishLocalStorageProvider.getItems()
  },

  clear(state) {
    DishLocalStorageProvider.deleteAll()
    state.items = []
  },
}

export const getters = {
  totals(state) {
    // Есть проблема вычисления при рендеринге, переписать на action
    let result = {
      xe: 0,
      carbohydrates: 0,
      protein: 0,
      fat: 0,
      calories: 0,
    }
    
    if (state.items.length > 0) {
      state.items.forEach(dishItem => {
        let dishParams = dishItem.params
        result.xe += dishParams.xe
        result.carbohydrates += dishParams.carbohydrates
        result.protein += dishParams.protein
        result.fat += dishParams.fat
        result.calories += dishParams.calories
      });
    }
    return result
  }
  
}
